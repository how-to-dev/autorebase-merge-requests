#!/usr/bin/env node

const fetch = require("node-fetch");

const projectId = process.argv[2] ? process.argv[2] : process.env.CI_PROJECT_ID,
  apiToken = process.argv[3] ? process.argv[3] : process.env.API_TOKEN,
  apiV4Url = process.env.CI_API_V4_URL
    ? process.env.CI_API_V4_URL
    : "https://gitlab.com/api/v4";

function callApi(command, method = "GET") {
  console.log("query", apiV4Url + "/projects/" + projectId + command);

  return fetch(apiV4Url + "/projects/" + projectId + command, {
    method,
    headers: { "PRIVATE-TOKEN": apiToken },
  });
}

callApi("/merge_requests?state=opened")
  .then((response) => response.json())
  .then((response) => {
    const iids = response.map((mr) => mr.iid);

    return Promise.all(
      iids.map((iid) => {
        return callApi(`/merge_requests/${iid}/rebase`, "PUT")
          .then((response) => response.json())
          .then((response) => {
            response.iid = iid;

            return response;
          })
          .catch(console.error);
      })
    );
  })
  .then((resultSummary) => {
    console.log(resultSummary);
  })
  .catch((error) => {
    console.error(error);
  });
